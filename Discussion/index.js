console.log("hello from JS"); //MESSAGE

//[SECTION] CONTROL STRUCTURES


//[SUB SECTION]: IF-ELSE STATEMENT

let numA = 3; //we will try to assess the value.

//If statement (Branch)
//The task of the if statement is to execute a procedure/action if the specified condition is "true".
if (numA <= 0) {
	//truthy branch
	//this block of code will run if the condition is MET.
	console.log('The condition was MET!'); 
}

//let name = 'Lorenzo'; //Allowed
//Maria => NOt allowed

let isLegal = false; 
//! -> NOT //false

//create a control structure that will allow the user to proceed if the value matches/passes the condition.
if (isLegal) {
	//if the passes the condition the "truthy" branch will run
	console.log('User can proceed!');
}

//[SUB SECTION] Else statement

// => This executed a statement if ALL other conditions are "FALSE" and/or has failed.

//lets create a control structure that will allow us to simulate a user login.


//prompt box -> prompt(): this will allow us to display a prompt dialog box which we can the user for an input.

//syntax: prompt(text/message[REQUIRED], placeholder/default text[OPTIONAL]);
//prompt("Please enter your first name:", "Martin"); 

//lets create a control structure that will allow us to check of the user is old enough to drink,
let age = 21; 

//alert() => display a message box to the user which would require their attention
// if (age >= 18) {
// 	// "truthy" brach
// 	alert("Your old enough to drink");
// } else {
// 	//"falsy" branch
// 	alert("Come back another day!");
// }


// lets create a control structure that will ask for the number of drinks that you will order. 

//ask the user how many drinks he wants
// let order = prompt('How many orders of drinks do you like?');

//convert the string data type into a number.
//parseInt() => will allow us to convert strings into integers

//create a logic that will make sure that the user's input is greater than 0

//Type coercion => conversion data type was converted to another data type.

//composite -> multiple data 
//primitive -> single data 

//1. if one of the operands in an object, it will be converted into a primitive data type (str, number, boolean).
//2. if at least 1 operand is a string it will convert the other operand into a string as well. 
//3. if both numbers are numbers then an arithmetic operation will be executed.

//in JS there 3 ways to multiply a string. 
   //1. repeat() method => this will allow us to return a new string value that contains the number of copies of the string. 
      //syntax: str.repeat(value/number)

   //2. loops => for loop
   //3. loops => while loop method.

// x + y 
// if (order > 0) {
//   console.log(typeof order);
//   let cocktail = "🍸"; 
//   alert(cocktail.repeat(order));
//   //alert(order * "🍸"); 
//     //multiplication	
// } else {
// 	alert('The number should be above 0');
// }

//You want to create other predetermined conditions you can create a nested (multiple) if-else statement. 



// mini task: vaccine checker

// Adjacent Skills
function vaccineChecker() {
   // ask information from the user.
   let vax = prompt('What brand is your vaccine?')
   //pfizer, PFIZER
   //So wee need to process the input of the user so that whatever input he will enter we can control the uniformity of the character casing. 
   //toLowerCase() -> will allow us to convert a string into all lowercase characters. (syntax: string.toLowerCase() )

   vax = vax.toLowerCase();
        //create a logic that will allow us to check the values inserted by the user to match the given parameters.
   if (vax === 'pfizer' || vax === 'moderna' || vax === 'astrazenica' || vax === 'janssen') {
       //display the response back to the client.
       alert(vax + ' is allowed to travel');
       } else {
           alert('sorry not permitted');
       }
   }
   vaccineChecker();
   //for this mini task we want the user to be able to invoke this function with the user of a trigger.
   //vaccineChecker();

   //onclick =>is an example of a JS Event. this "event" in JS occurs when the user clicks on an element/component to trigger a certain procedure. 

   //syntax: <element/component onclick="myScript/Function" > 


   // Typhoon Checker

   function determineTyphoonIntensity() {
       //going to need an input from the user.
           //we need a 'number' data type so that we can properly compare the values.

    let windspeed = parseInt(prompt('Wind speed: '));
    console.log(typeof windspeed);

    if (windspeed <= 29) {
       alert('Not a Typhoon yet!')
           //for this logic/structure .. it will only assess the whole number.


           // Condition 2
        } else if (windspeed >= 30 && windspeed <= 61) {
           alert('Tropical Depression Detected');
           }
               // Condition 3
               else if (windspeed >= 62 && windspeed <= 88) {
                   alert('Tropical Storm Detected');
                   }
                   // Condition 4
                   else if (windspeed >= 89 && windspeed <= 117) {
                       alert('Severe Tropical Storm');
                       }
                   // Default
               else {
                   alert('Typhoon Detected'); 
               }

   }
   determineTyphoonIntensity();

   // Conditional Ternary Operator
   // is still follows the same syntax wth an if-else
   // This is the only JS operator that takes 3 operands
   // '?' question mark => this would describe a condition that if resulted to "true" will run truthy
   // : colon => this is symbol will seperate the truthy and falsy statements

   function ageChecker() {
       let age = parseInt(prompt('How old are you?'));

       //syntax: condition ? "truthy" : "falsy"
       // return = shortcut of if else
        return  (age >= 18) ? alert('Old enough to vote') : alert('Not yet old enough');

       //  Equivalent in if else:
           // if (age >= 18) {
           //   //truthy
           //   alert('Old enough to vote');
           // } else {
           //   //falsy
           //   alert('Not yet old enough');
           // }

          
   }

   // NOTE: Conditional Chains
   // function example(…) {
   //     return condition1 (--> if) ? value1 (--> result if true)
   //          : condition2 (--> if else) ? value2 (--> value if true)
   //          : condition3 (--> if else) ? value3 (--> value if true)
   //          : value4 (--> else);
   // }

   // Equivalent to:

   // function example(…) {
   //     if (condition1) { return value1; }
   //     else if (condition2) { return value2; }
   //     else if (condition3) { return value3; }
   //     else { return value4; }
   // }

//create a function that will determin the owner of a computer unit

function determineComputerOwner()
   {
       let unit = prompt('What is the unit no. ?');//string => number 
       let manggagamit;
       //the unit -> represents the unit number.
       //unit === case 
       //the manggagamit -> represent the user who owns the unit.
       switch (unit) {
       //declare multiple cases to represent each outcome that will match the value inside the expression.
            case '1':
                manggagamit = "John Travolta";
                break;
            case '2':
                manggagamit = 'Steve Jobs';
                break;
            case '3': 
                manggagamit = 'Sid Meier';
                break;
            case '4': 
                manggagamit = 'Onel de guzman';
                break;
            case '5': 
                manggagamit = 'Christian Salvador';
                break;
            default: 
           manggagamit = 'wala yan sa options na pagpipilian';
           //if all else fails or if non of the preceeding cases above meets the expression, this statement will become the fail safe/default response.
       }
   
       alert(manggagamit); 
 }
 
 determineComputerOwner();
 
 //When to use "" (double quotes) OVER '' (single quotes)
 
 //=> "name" === 'name'.
 
 //we can use either methods to declare a string value. however we can use one over the other to ESCAPE the scope of the initial symbol. 
 
//   let dialog = "'Drink your water bhie' - mimiyuhh says";
//   let dialog2 = ' "I shall return" - McArthur '; 
//   let ownership = ' Aling Nena\'s Tindahan '//alternative in inserting apostrophe.
//   console.log(ownership); 
 

//create a function that will determine if the age is too old for preschool
function edadChecker(){
    //we are going to use a try-catch statement instead
  
    //get the input of the user.
    //the getElementById() will target a component within the document using its ID attribute
    //the "document" parameter describes the HTML document/file where the JS module is linked.
    // "value" => describes the value property of our elements
    let userInput = document.getElementById('age').value; 
    //alert(userInput); //checker
    //we will now target the element where we will display the output of this function.
    let message = document.getElementById('outputDisplay'); 
    console.log(typeof userInput); //string
    
    try {
        //lets make sure that the input inserted by the user is NOT equals to a blank string.
        //throw -> this statement examines the input and returns an error.
        if (userInput === '' ) throw 'the input is empty';
        //create a conditional statement that will check if the input in NOT a number 
        //in order to check if the value is NOT A NUMBER, We will use a isNaN()
        if (isNaN(userInput)) throw 'the input is Not a Number';
        if (userInput <= 0) throw 'Not a valid Input' 
        if (userInput <= 7) throw 'the input is good for preschool'; 
        if (userInput > 7) throw 'too old for preschool';
    } catch(err) {
       //the "err" is to define the error that will be thrown by the try section. so "err" is caught by the catch statement and a custom error message will be displayed.
       //how are we going to inject a value inside the html container?
       //=> using innerHTML property : 
       //syntax: element.innerHTML -> This will allow us to get/set the HTML markup contained within the element
       message.innerHTML = "Age Input: " + err; 
    } finally {
        //this statement here will be executed regardless of the result above.
        console.log('This is from the finally section');
        //lalabas both ung block of code indicated in the finally section including the outcome of the try statement.
    }
  
  }
  
  