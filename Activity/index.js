// // // TASK 1: Create a function that will prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
// // // a. If the total of the two numbers is less than 10, add the numbers
// // // b. If the total of the two numbers is 10 - 20, subtract the numbers
// // // c. If the total of the two numbers is 21 - 29 multiply the numbers
// // // d. If the total of the two numbers is greater than or equal to 30, divide the numbers
// // // e. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.

function determineTwoNumber()
    {
        let numA = parseInt(prompt('Input a number: '));
        let numB = parseInt(prompt('Input another number: '));
        let total = parseInt(numA + numB);
        console.log(typeof total);
        let difference = parseInt(numA - numB);
        let product = parseInt(numA * numB);
        let quotient = parseInt(numA / numB);

        if (total == 9 ) 
            {
                console.warn('The sum of the two numbers are: ' + total);
            } 
                else if (total >10 && total <= 20) 
                    {
                        alert('The difference of the numbers are: ' + difference);
                    }
                        else if (total >20 && total < 29) 
                            {
                                alert('The product of the two numbers are: ' + product);
                            }
                                else if (total >= 29) 
                                {
                                    alert('The quotient of the two numbers are: ' + quotient);
                                }  
                                    else {
                                            alert('The sum of the two numbers are: ' + total);
                                        }

        
    }
    determineTwoNumber();

// // TASK 2. Create a function that will prompt the user for their name and age and print out different alert messages based on the user input:
// //   -> If the name OR age is blank/null, print the message are you a time traveler?
// //  -> If the name AND age is not blank, print the message with the user’s name and age.


function alienChecker() {

    let name = prompt('What is your name ?: ');
    let age = parseInt(prompt('What is your age ?: '));
    
    if ( name === "" || name === null || age === "" || age === null) 
        {
            alert('Are you a time traveler?');
        
            } 
  
            else 
            {
                 alert('Hello ' + name + "." + " " + 'Your age is ' + age + ".");
                }
}

alienChecker();


// TASK 3: Create a function with switch case statement that will check if the user's age input is within a certain set of expected input:
// - 18 - print the message You are now allowed to party.
// - 21 - print the message You are now part of the adult society.
// - 65 - print the message We thank you for your contribution to society.
// - Any other value - print the message Are you sure you're not an alien?

function getAgeStatus() {

    let edad = parseInt(prompt('What is your age. ?'));//string => number 

    switch (edad) {
        case 18:
            alert("You are not allowed to party.");
            break;
        case 21:
            alert("You are now part of the adult society.");
            break;
        case 65:
            alert("We thank you for your contribution to society.");
            break;
        default:
            alert("Are you sure you're not an alien?");
            break;
    }
}
getAgeStatus();
